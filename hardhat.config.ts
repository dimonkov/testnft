import { HardhatUserConfig } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";
import "dotenv/config";
import { env } from "process";

const config: HardhatUserConfig = {
  solidity: "0.8.9",
  networks: {
    hardhat: {},
    moonbase: {
      url: "https://rpc.api.moonbase.moonbeam.network",
      accounts: [env.DEPLOYER_ACC!]
    }
  },
  etherscan: {
    apiKey: {
      moonbaseAlpha: env.MOONBASE_ETHERSCAN_API_KEY!
    }
  }
};

export default config;
